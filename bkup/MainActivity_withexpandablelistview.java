package com.antarix.torchlight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.WindowCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	public static String INTENT_EXT_WEB_COOKIE = "com.antarix.torchlight.webcookie"; 

	private WebView mWebView; 
	private String mCookie = null;
	
	private DrawerLayout mDrawerLayout;
	private ExpandableListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private LinearLayout mDrawer;
	private List<HashMap<String,String>> mList ;
	private SimpleExpandableListAdapter mAdapter;
	final private String LABEL = "label";
	final private String ICON = "icon";
	final private String LINK = "link";
	final private String LAYOUT = "layout";
	
	int mPosition = -1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		supportRequestWindowFeature(WindowCompat.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_main);
		
		mWebView = (WebView)findViewById(R.id.webView);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.clearCache(true);
		
		mWebView.setWebViewClient(new WebViewClient() {
	        @Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {
	            if (url.contains("/mobile/login")){
	            	// ToDo: login in background
	            	Log.d("Torchlight", "Login required");
	            	doSignin();
	            	return true;
	            }
	            
	            if (url.contains("/a/") && !url.contains("/mobile/a/")){
	            	Log.d("Torchlight", "url:" + url);
	            	url = url.replace("/a/", "/mobile/a/");
	            	Log.d("Torchlight", "url changed to:" + url);
	            }
	            view.loadUrl(url);
	            return false;
	        }
	    
	        @Override
	        public void onReceivedError(WebView view, int errorCode,
	        		String description, String failingUrl) {
	        	super.onReceivedError(view, errorCode, description, failingUrl);
	        	Log.d("Torchlight", "OnError:" + errorCode + ", url:" + failingUrl);
	        }
	        
	        @Override
	        public void onLoadResource(WebView view, String url) {
	        	super.onLoadResource(view, url);
	        	Log.d("Torchlight", "url:" + url);
	        }
	    });		
		
		// Getting a reference to the drawer listview
		mDrawerList = (ExpandableListView) findViewById(R.id.drawer_list);
 
		// Getting a reference to the sidebar drawer ( Title + ListView )
		mDrawer = ( LinearLayout) findViewById(R.id.drawer);
 
		
		List<Map<String, String>> groupData = new ArrayList<Map<String, String>>() {{
			  add(new HashMap<String, String>() {{
				put(LABEL, "Activity");
				put(ICON, getResources().getString(R.string.fa_shopping_cart));
				put(LINK, "traffic");
			  }});
			  add(new HashMap<String, String>() {{
				put(LABEL, "Consumers");
				put(ICON, getResources().getString(R.string.fa_users));
				put(LINK, "consumers-dashboard");
			  }});
			}};		
		
		// Each row in the list stores country name, count and flag
		mList = new ArrayList<HashMap<String,String>>();
		HashMap<String, String> hm = new HashMap<String,String>();
		hm.put(LABEL, "Activity");
		hm.put(ICON, getResources().getString(R.string.fa_shopping_cart));
		hm.put(LINK, "traffic");
		mList.add(hm);
		
		hm = new HashMap<String,String>();
		hm.put(LABEL, "Consumers");
		hm.put(ICON, getResources().getString(R.string.fa_users));
		hm.put(LINK, "consumers-dashboard");
		mList.add(hm);

		hm = new HashMap<String,String>();
		hm.put(LABEL, "Stores");
		hm.put(ICON, getResources().getString(R.string.fa_map_marker));
		hm.put(LINK, "STORES");
		mList.add(hm);
		
		hm = new HashMap<String,String>();
		hm.put(LABEL, "Settings");
		hm.put(ICON, getResources().getString(R.string.fa_cog));
		hm.put(LINK, "SETTINGS");
		hm.put(LAYOUT, Integer.toString(R.layout.menu_footer_layout));
		mList.add(hm);

		hm = new HashMap<String,String>();
		hm.put(LABEL, "Help");
		hm.put(ICON, getResources().getString(R.string.fa_question_circle));
		hm.put(LINK, "HELP");
		hm.put(LAYOUT, Integer.toString(R.layout.menu_footer_layout));
		mList.add(hm);
		
		// Keys used in Hashmap
		String[] from = { ICON, LABEL };
		 
		// Ids of views in listview_layout
		int[] to = { R.id.icon , R.id.text};
		
		List<List<Map<String, String>>> listOfChildGroups = new ArrayList<List<Map<String, String>>>();
		List<Map<String, String>> childGroupForFirstGroupRow = new ArrayList<Map<String, String>>(){{
			}};
		listOfChildGroups.add(childGroupForFirstGroupRow);

		List<Map<String, String>> childGroupForSecondGroupRow = new ArrayList<Map<String, String>>(){{
		  add(new HashMap<String, String>() {{
			put(LABEL, "Consumers");
			put(ICON, getResources().getString(R.string.fa_users));
			put(LINK, "consumers-dashboard");
		  }});
		}};
		listOfChildGroups.add(childGroupForSecondGroupRow);		 
		// Instantiating an adapter to store each items
		// R.layout.drawer_layout defines the layout of each item
		mAdapter = new SimpleExpandableListAdapter(this, groupData, R.layout.menu_item_layout,
				from, to, listOfChildGroups, R.layout.menu_item_layout, from, to){

			@Override
			public View getGroupView(int groupPosition,
					boolean isExpanded, View convertView,
					ViewGroup parent) {
				View v = super.getGroupView(groupPosition, isExpanded, convertView, parent);
				TextView cv = (TextView)v.findViewById(R.id.icon);
				Typeface tf = Typefaces.get(v.getContext(), "fontawesome-webfont.ttf");
				cv.setTypeface(tf);
				return v;
			}
			
			@Override
			public View getChildView(int groupPosition,
					int childPosition, boolean isLastChild,
					View convertView, ViewGroup parent) {
				// TODO Auto-generated method stub
				View v = super.getChildView(groupPosition, childPosition, isLastChild,
						convertView, parent);
				
				TextView cv = (TextView)v.findViewById(R.id.icon);
				Typeface tf = Typefaces.get(v.getContext(), "fontawesome-webfont.ttf");
				cv.setTypeface(tf);
				return v;
			}
		};
		/*mAdapter = new SimpleAdapter(this, mList, R.layout.menu_item_layout, from, to){
		
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				
				View v = null;
				HashMap<String, String> item = (HashMap<String, String>)getItem(position);
				String layout = item.get(LAYOUT);  
				if (layout != null){
					LayoutInflater inflater = getLayoutInflater();
					View view = inflater.inflate(Integer.parseInt(layout), parent, false);
					v = super.getView(position, view, parent);
				}else{
					v = super.getView(position, convertView, parent);
				}
				TextView cv = (TextView)v.findViewById(R.id.icon);
				Typeface tf = Typefaces.get(v.getContext(), "fontawesome-webfont.ttf");
				cv.setTypeface(tf);
				return v;
			}
		};*/
		 
		// Getting reference to DrawerLayout
		mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		 
		// Creating a ToggleButton for NavigationDrawer with drawer event listener
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, 
				R.drawable.ic_drawer , R.string.drawer_open, R.string.drawer_close){
		 
			 /** Called when drawer is closed */
			 public void onDrawerClosed(View view) {
				 supportInvalidateOptionsMenu();
			 }
		 
			 /** Called when a drawer is opened */
			 public void onDrawerOpened(View drawerView) {
				 supportInvalidateOptionsMenu();
			 }
		};		
		
		// Setting event listener for the drawer
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		 
		// ItemClick event handler for the drawer items
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
		 
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

			/*HashMap<String, String> item = mList.get(position);
			String link = item.get(LINK);
			if (link.contentEquals("HELP")){
				// TODO: show help view here
			}else if (link.contentEquals("SETTINGS")){
				// TODO: show settings view here
			}else{
				mWebView.loadUrl("http://www.torchlight.in/mobile/a/" + link);
			}*/
			// Closing the drawer
			mDrawerLayout.closeDrawer(mDrawer);
		}
		});
		
		// Enabling Up navigation
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		 
		// Setting the adapter to the listView
		mDrawerList.setAdapter(mAdapter);
		
		mCookie = getIntent().getExtras().getString(INTENT_EXT_WEB_COOKIE);
		new WebViewTask().execute();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}	
	
	@Override
	public void onBackPressed() {
		if(mWebView.canGoBack())
	        mWebView.goBack();
	    else
	    {
	    	mWebView.clearCache(true);
	    	super.onBackPressed();
	    }
	}	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return mDrawerToggle.onOptionsItemSelected(item);
	}
	
	private void doSignin(){
		final String emailId = AppPreferences.get(this, AppPreferences.USERNAME);
		final String password = AppPreferences.get(this, AppPreferences.PASSWORD);

		if (NetworkUtil.TYPE_NOT_CONNECTED == NetworkUtil.getConnectivityStatus(this)){
    		//ToDo: Show connection prompt and ask to exit/retry
			Toast.makeText(getApplicationContext(), 
                    "No data connection available", Toast.LENGTH_LONG).show();
			return;
		}
		
		final AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		params.put("username", emailId);
		params.put("password", password);
		
		client.post("http://www.torchlight.in/mobile/login", params, new AsyncHttpResponseHandler() {
		    
			private String cookie = "";
			private int mStatusCode = 0;
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] response) {
				super.onSuccess(statusCode, headers, response);
		    	/*Log.d("Torchlight", "OnSuccess statusCode:" + statusCode);
		    	for (int i = 0; i < headers.length; i++){
		    		Log.d("Torchlight", "header: " + headers[i].getName() + ":" + headers[i].getValue());
		    	}
		    	
		    	String s = new String(response);
		    	Log.d("Torchlight", "response:" + s);
		    	*/
				mStatusCode = statusCode;
	    		String val = getHeaderValue(headers, "Set-Cookie");
	    		if (val.length() > 0){
	    			String[] arr = val.split(";");
	    			cookie = arr[0];
	    		}
			}
			
		    @Override
		    public void onFinish() {
		    	super.onFinish();
	    		
	    		if (mStatusCode == 0){
		    		Toast.makeText(getApplicationContext(), 
		                    "Data connection not available", Toast.LENGTH_LONG).show();
	    		}else if (mStatusCode == 401){
	    			onSigninFailed();
		    	}else if (mStatusCode == 200 && cookie.length() > 0){
		    		onSignedIn(cookie);
		    	}else{
		    		Toast.makeText(getApplicationContext(), 
		                    "Service unavailable at this time. Please try again later.", Toast.LENGTH_LONG).show();
		    	}
		    }
		    
		    @Override
		    public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable arg3) {
		    	super.onFailure(statusCode, headers, response, arg3);
		    	mStatusCode = statusCode;
		    	/*Log.d("Torchlight", "OnFailure statusCode:" + statusCode);
		    	
		    	for (int i = 0; i < headers.length; i++){
		    		Log.d("Torchlight", "header: " + headers[i].getName() + ":" + headers[i].getValue());
		    	}*/
		    }
		    
		    private String getHeaderValue(Header[] headers, String headerName){
		    	String value = "";
		    	for (int i = 0; i < headers.length; i++){
		    		if (headers[i].getName().contentEquals(headerName)){ 
		    			value = headers[i].getValue();
		    			break;
		    		}
		    	}
		    	return value;
		    }
		});
	}

	private void onSignedIn(String cookie){
		mCookie = cookie;
		AppPreferences.set(this, AppPreferences.WEBCOOKIE, cookie);
		new WebViewTask().execute();
	}
	
	private void onSigninFailed(){
		Toast.makeText(getApplicationContext(), 
                "Incorrect username and/or password", Toast.LENGTH_LONG).show();

		AppPreferences.set(this, AppPreferences.WEBCOOKIE, "");
		Intent intent = new Intent(this, SigninActivity.class);
		startActivity(intent);
		finish();
	}
	
	private class WebViewTask extends AsyncTask<Void, Void, Boolean> {
		String sessionCookie;
		CookieManager cookieManager;
 
	        @Override
		protected void onPreExecute() {
			CookieSyncManager.createInstance(MainActivity.this);
			cookieManager = CookieManager.getInstance();
			cookieManager.removeSessionCookie(); 
			super.onPreExecute();
		}
		protected Boolean doInBackground(Void... param) {
            /* this is very important - THIS IS THE HACK */
			SystemClock.sleep(1000);
			return false;
		}
		@Override
		protected void onPostExecute(Boolean result) {
			if (mCookie != null) {
				cookieManager.setCookie("www.torchlight.in", mCookie + ";domain=torchlight.in");
				CookieSyncManager.getInstance().sync();
			}
			WebSettings webSettings = mWebView.getSettings();
			webSettings.setJavaScriptEnabled(true);
			webSettings.setBuiltInZoomControls(true);
			/*mWebView.setWebViewClient(new WebViewClient() {
				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					return super.shouldOverrideUrlLoading(view, url);
				}
				
			});*/
			mWebView.loadUrl("http://www.torchlight.in/mobile/a/stores-manage");
		}
	}	
}
