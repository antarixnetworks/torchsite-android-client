package com.antarix.torchlight;

import org.apache.http.Header;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.WindowCompat;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SigninActivity extends ActionBarActivity {
	
	private EditText mEditEmail;
	private EditText mEditPassword;
	private Button mBtnSignin;
	private View mStatusBar;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		supportRequestWindowFeature(WindowCompat.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_signin);
	
		mEditEmail = (EditText)findViewById(R.id.editEmail);
		mEditPassword = (EditText)findViewById(R.id.editPassword);
		mBtnSignin = (Button)findViewById(R.id.btnSignin);
		mStatusBar = findViewById(R.id.statusBar);
		
		mEditEmail.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// Nothing to do here
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// Nothing to do here
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if (TextUtils.isEmpty(s) || TextUtils.isEmpty(mEditPassword.getText())){
					mBtnSignin.setEnabled(false);
				}else{
					mBtnSignin.setEnabled(true);
				}
			}
		});
		
		mEditEmail.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus && TextUtils.isEmpty(mEditEmail.getText())){
					mEditEmail.setError("This field cannot be blank");
				}
			}
		});

		mEditPassword.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// Nothing to do here
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// Nothing to do here
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if (TextUtils.isEmpty(s) || TextUtils.isEmpty(mEditEmail.getText())){
					mBtnSignin.setEnabled(false);
				}else{
					mBtnSignin.setEnabled(true);
				}
			}
		});

		mEditPassword.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus && TextUtils.isEmpty(mEditPassword.getText())){
					mEditEmail.setError("This field cannot be blank");
				}
			}
		});
		
		mBtnSignin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				doSignin();
			}
		});
		
		mEditEmail.setText(AppPreferences.get(this, AppPreferences.USERNAME));
		mEditPassword.setText(AppPreferences.get(this, AppPreferences.PASSWORD));
	}

	private void doSignin(){
		final String emailId = mEditEmail.getText().toString();
		final String password = mEditPassword.getText().toString();

		View v = this.getCurrentFocus();
	    if(v != null){
			InputMethodManager imm = (InputMethodManager)getSystemService(
			      Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(v.getWindowToken(), 0);		
	    }
		
		if (NetworkUtil.TYPE_NOT_CONNECTED == NetworkUtil.getConnectivityStatus(this)){
    		Toast.makeText(getApplicationContext(), R.string.toast_connection_unavailable, 
                    Toast.LENGTH_LONG).show();
			return;
		}
		
		disableInput();
		showStatusBar();
		
		final AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		params.put("username", emailId);
		params.put("password", password);

		PersistentCookieStore myCookieStore = new PersistentCookieStore(this);
		client.setCookieStore(myCookieStore);
		myCookieStore.clear();
		
		client.post("http://www.torchlight.in/mobile/login", params, new AsyncHttpResponseHandler() {
		    
			private String cookie = "";
			private int mStatusCode = 0;
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] response) {
				super.onSuccess(statusCode, headers, response);
		    	/*Log.d("Torchlight", "OnSuccess statusCode:" + statusCode);
		    	for (int i = 0; i < headers.length; i++){
		    		Log.d("Torchlight", "header: " + headers[i].getName() + ":" + headers[i].getValue());
		    	}
		    	
		    	String s = new String(response);
		    	Log.d("Torchlight", "response:" + s);
		    	 */
				mStatusCode = statusCode;
	    		String val = getHeaderValue(headers, "Set-Cookie");
	    		if (val.length() > 0){
	    			String[] arr = val.split(";");
	    			cookie = arr[0];
	    		}
			}
			
		    @Override
		    public void onFinish() {
		    	super.onFinish();
		    	hideStatusBar();
	    		enableInput();
	    		
	    		if (mStatusCode == 0){
		    		Toast.makeText(getApplicationContext(),
		    				R.string.toast_connection_unavailable,
		                    Toast.LENGTH_LONG).show();
	    		}else if (mStatusCode == 401){
		    		Toast.makeText(getApplicationContext(), 
		    				R.string.toast_authentication_failed,
		    				Toast.LENGTH_LONG).show();
		    	}else if (mStatusCode == 200 && cookie.length() > 0){
		    		onSignedIn(emailId, password, cookie);
		    	}else{
		    		Toast.makeText(getApplicationContext(),
		    				R.string.toast_server_error,
		                    Toast.LENGTH_LONG).show();
		    	}
		    }
		    
		    @Override
		    public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable arg3) {
		    	super.onFailure(statusCode, headers, response, arg3);
		    	mStatusCode = statusCode;
		    	Log.d("Torchlight", "OnFailure statusCode:" + statusCode);
		    	
		    	/*for (int i = 0; i < headers.length; i++){
		    		Log.d("Torchlight", "header: " + headers[i].getName() + ":" + headers[i].getValue());
		    	}*/
		    }
		    
		    private String getHeaderValue(Header[] headers, String headerName){
		    	String value = "";
		    	for (int i = 0; i < headers.length; i++){
		    		if (headers[i].getName().contentEquals(headerName)){ 
		    			value = headers[i].getValue();
		    			break;
		    		}
		    	}
		    	return value;
		    }
		});
	}
	
	private void disableInput(){
		mEditEmail.setEnabled(false);
		mEditPassword.setEnabled(false);
		mBtnSignin.setEnabled(false);
	}
	
	private void enableInput(){
		mEditEmail.setEnabled(true);
		mEditPassword.setEnabled(true);
		mBtnSignin.setEnabled(true);
	}

	private void showStatusBar(){
		mStatusBar.setVisibility(View.VISIBLE);
	}

	private void hideStatusBar(){
		mStatusBar.setVisibility(View.GONE);
	}

	private void onSignedIn(String username, String password, String cookie){
		
		AppPreferences.set(this, AppPreferences.USERNAME, username);
		AppPreferences.set(this, AppPreferences.PASSWORD, password);
		AppPreferences.set(this, AppPreferences.WEBCOOKIE, cookie);
		
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
		finish();
	}
}
