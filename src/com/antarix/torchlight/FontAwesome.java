package com.antarix.torchlight;

import java.io.InputStream;
import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class FontAwesome {

	private static final Hashtable<String, String> keys = new Hashtable<String, String>();
	
	public static String getValue(Context c, String key) {
		synchronized (keys) {
			if (keys.size() == 0) {
		        try{
		        	InputStream is = c.getAssets().open("font_awesome_keys.json");
		        	int size = is.available();
		        	byte[] buffer = new byte[size];
		        	is.read(buffer);
		        	is.close();
		        	String json = new String(buffer, "UTF-8");
		        	
		        	JSONArray arr = new JSONArray(json);

		            for (int i = 0; i < arr.length(); i++){
		               JSONObject o = arr.getJSONObject(i);
		               keys.put(o.getString("name"), new String(Character.toChars(Integer.decode(o.getString("value")))));
		            }		        	
		        	
		        }catch(Exception ex){
		        	Log.w("Torchlight", "Unable to read font_awesome_keys.json", ex);
		        }
			}
			
			return keys.get(key);
		}
	}
}
