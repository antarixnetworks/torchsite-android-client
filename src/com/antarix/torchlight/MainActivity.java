package com.antarix.torchlight;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.WindowCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private WebView mWebView;
	private View mProgressBarLayout;
	private String mCookie = null;
	
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private LinearLayout mDrawer;
	private List<HashMap<String,String>> mMenuList = new ArrayList<HashMap<String,String>>();
	private List<List<HashMap<String, String>>> mSubmenuData = new ArrayList<List<HashMap<String, String>>>();
	private MenuAdapter mAdapter;
	final private String LABEL = "label";
	final private String ICON = "icon";
	final private String LINK = "link";
	
	private List<String> mPageTitles; 
	
	int mPosition = -1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		supportRequestWindowFeature(WindowCompat.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_main);
		
		mPageTitles = new ArrayList<String>();
		
		mPageTitles.add("Manage Stores");
		
		mWebView = (WebView)findViewById(R.id.webView);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.clearCache(true);
		
		mProgressBarLayout = findViewById(R.id.progressbar_layout);
		
		mWebView.setWebViewClient(new WebViewClient() {
	        @Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {
	            if (url.contains("/mobile/login")){
	            	doSignin();
	            	return true;
	            }
	            
	            String loadUrl = url;
	            if (url.contains("/a/") && !url.contains("/mobile/a/")){
	            	Log.d("Torchlight", "url:" + url);
	            	loadUrl = url.replace("/a/", "/mobile/a/");
	            	Log.d("Torchlight", "url changed to:" + loadUrl);
	            	view.loadUrl(loadUrl);
	            	return true;
	            }
	            
	            return false;
	        }
	    
	        @Override
	        public void onReceivedError(WebView view, int errorCode,
	        		String description, String failingUrl) {
	        	super.onReceivedError(view, errorCode, description, failingUrl);
	        	Log.d("Torchlight", "OnError:" + errorCode + ", url:" + failingUrl);
	        	view.loadData("<html></html>", "text/html; charset=UTF-8", null);
	    		Toast.makeText(getApplicationContext(), 
	    				R.string.toast_connection_unavailable, Toast.LENGTH_LONG).show();
	        }
	        
	        @Override
	        public void onLoadResource(WebView view, String url) {
	        	super.onLoadResource(view, url);
	        	//Log.d("Torchlight", "Resource url:" + url);
	        }
	        
	        @Override
	        public void onPageStarted(WebView view, String url, Bitmap favicon) {
	        	super.onPageStarted(view, url, favicon);
	        	
	        	//Log.d("Torchlight", "Titles: " + mPageTitles.size() + ", current page:" + (mWebView.copyBackForwardList().getCurrentIndex() + 1));
	        	
	        	//Log.d("Torchlight", "Started url: " + url);
	        	
	        	/*if (mPageTitles.size() > 0){
	        		if (mPageTitles.size() == mWebView.copyBackForwardList().getSize()){
	        			getActionBar().setTitle(mPageTitles.get(mPageTitles.size()-1));
	        		}else{
	        			mPageTitles.add(mPageTitles.get(mPageTitles.size()-1));
	        		}
	        	}*/
	        	mProgressBarLayout.setVisibility(View.VISIBLE);
	        }
	        
	        @Override
	        public void onPageFinished(WebView view, String url) {
	        	super.onPageFinished(view, url);
	        	//Log.d("Torchlight", "Finished url: " + url);
	        	mProgressBarLayout.setVisibility(View.GONE);
	        }
	    });		
		
		// Getting a reference to the drawer listview
		mDrawerList = (ListView) findViewById(R.id.drawer_list);
 
		// Getting a reference to the sidebar drawer ( Title + ListView )
		mDrawer = ( LinearLayout) findViewById(R.id.drawer);
 
		// Each row in the list stores country name, count and flag
		HashMap<String,String> hm = new HashMap<String,String>();
		hm.put(LABEL, "Settings");
		hm.put(ICON, FontAwesome.getValue(this, "fa-cog"));
		hm.put(LINK, "SETTINGS");
		hm.put(MenuAdapter.LAYOUT_KEY, Integer.toString(R.layout.menu_footer_layout));
		mMenuList.add(hm);

		hm = new HashMap<String,String>();
		hm.put(LABEL, "Help");
		hm.put(ICON, FontAwesome.getValue(this, "fa-question-circle"));
		hm.put(LINK, "HELP");
		hm.put(MenuAdapter.LAYOUT_KEY, Integer.toString(R.layout.menu_footer_layout));
		mMenuList.add(hm);

		hm = new HashMap<String,String>();
		hm.put(LABEL, "Signout");
		hm.put(ICON, FontAwesome.getValue(this, "fa-sign-out"));
		hm.put(LINK, "SIGNOUT");
		hm.put(MenuAdapter.LAYOUT_KEY, Integer.toString(R.layout.menu_footer_layout));
		mMenuList.add(hm);
		
		String[] from = { ICON, LABEL };
		int[] to = { R.id.icon , R.id.text};
		 
		mAdapter = new MenuAdapter(this, mMenuList, R.layout.menu_item_layout, from, to, mSubmenuData, R.layout.menu_footer_layout);
		
		// Getting reference to DrawerLayout
		mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
		 
		// Creating a ToggleButton for NavigationDrawer with drawer event listener
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, 
				R.drawable.ic_drawer , R.string.drawer_open, R.string.drawer_close){
		 
			 /** Called when drawer is closed */
			 public void onDrawerClosed(View view) {
				 supportInvalidateOptionsMenu();
			 }
		 
			 /** Called when a drawer is opened */
			 public void onDrawerOpened(View drawerView) {
				 supportInvalidateOptionsMenu();
			 }
		};
		
		// Setting event listener for the drawer
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		 
		// ItemClick event handler for the drawer items
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {
		 
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

			HashMap<String, String> item = (HashMap<String, String>)mAdapter.getItem(position);
			String link = item.get(LINK);
			String label = item.get(LABEL);
			String childIndex = item.get(MenuAdapter.CHILD_INDEX_KEY);
			
			if (childIndex != null){
				if (item.containsKey(MenuAdapter.EXPANDED_KEY)){
					item.remove(MenuAdapter.EXPANDED_KEY);
				}else{
					item.put(MenuAdapter.EXPANDED_KEY, "1");
				}
				mAdapter.notifyDataSetChanged();
				return;
				
			}else if (link.contentEquals("HELP")){
				// TODO: show help view here
			}else if (link.contentEquals("SETTINGS")){
				showSettingsActivity();
			}else if (link.contentEquals("SIGNOUT")){
				doSignout();
			}else{
				loadPage(label, link);
			}
			// Closing the drawer
			mDrawerLayout.closeDrawer(mDrawer);
		}
		});
		
		// Enabling Up navigation
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		 
		// Setting the adapter to the listView
		mDrawerList.setAdapter(mAdapter);
		prepareMenu();
		
		mCookie = AppPreferences.get(this, AppPreferences.WEBCOOKIE);
		new WebViewTask().execute();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}	
	
	@Override
	public File getCacheDir() {
		return getApplicationContext().getCacheDir();
	}
	
	@Override
	protected void onDestroy() {
		mWebView.clearCache(true);
		super.onDestroy();
	}
	
	@Override
	public void onBackPressed() {
		if(mWebView.canGoBack()){
			if (mPageTitles.size() > 1){
				mPageTitles.remove(mPageTitles.size()-1);
			}
			mWebView.goBack();
		}else{
	    	super.onBackPressed();
	    }
	}	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return mDrawerToggle.onOptionsItemSelected(item);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	
	private void loadPage(String title, String path){
		//getSupportActionBar().setTitle(title);
		mPageTitles.add(title);
		mWebView.loadUrl("http://www.torchlight.in/mobile/a/" + path);
	}
	
	private void doSignin(){
		final String emailId = AppPreferences.get(this, AppPreferences.USERNAME);
		final String password = AppPreferences.get(this, AppPreferences.PASSWORD);

		final AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		params.put("username", emailId);
		params.put("password", password);
		
		PersistentCookieStore myCookieStore = new PersistentCookieStore(this);
		client.setCookieStore(myCookieStore);
		
		client.post("http://www.torchlight.in/mobile/login", params, new AsyncHttpResponseHandler() {
		    
			private String cookie = "";
			private int mStatusCode = 0;
			
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] response) {
				super.onSuccess(statusCode, headers, response);
		    	/*Log.d("Torchlight", "OnSuccess statusCode:" + statusCode);
		    	for (int i = 0; i < headers.length; i++){
		    		Log.d("Torchlight", "header: " + headers[i].getName() + ":" + headers[i].getValue());
		    	}
		    	
		    	String s = new String(response);
		    	Log.d("Torchlight", "response:" + s);
		    	*/
				mStatusCode = statusCode;
	    		String val = getHeaderValue(headers, "Set-Cookie");
	    		if (val.length() > 0){
	    			String[] arr = val.split(";");
	    			cookie = arr[0];
	    		}
			}
			
		    @Override
		    public void onFinish() {
		    	super.onFinish();
	    		
	    		if (mStatusCode == 0){
		    		Toast.makeText(getApplicationContext(), R.string.toast_connection_unavailable, 
		                    Toast.LENGTH_LONG).show();
	    		}else if (mStatusCode == 401){
	    			onSigninFailed();
		    	}else if (mStatusCode == 200 && cookie.length() > 0){
		    		onSignedIn(cookie);
		    	}else{
		    		Toast.makeText(getApplicationContext(), 
		                    "Service unavailable at this time. Please try again later.", Toast.LENGTH_LONG).show();
		    	}
		    }
		    
		    @Override
		    public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable arg3) {
		    	super.onFailure(statusCode, headers, response, arg3);
		    	mStatusCode = statusCode;
		    	/*Log.d("Torchlight", "OnFailure statusCode:" + statusCode);
		    	
		    	for (int i = 0; i < headers.length; i++){
		    		Log.d("Torchlight", "header: " + headers[i].getName() + ":" + headers[i].getValue());
		    	}*/
		    }
		    
		    private String getHeaderValue(Header[] headers, String headerName){
		    	String value = "";
		    	for (int i = 0; i < headers.length; i++){
		    		if (headers[i].getName().contentEquals(headerName)){ 
		    			value = headers[i].getValue();
		    			break;
		    		}
		    	}
		    	return value;
		    }
		});
	}

	private void doSignout(){
		
		if (NetworkUtil.TYPE_NOT_CONNECTED == NetworkUtil.getConnectivityStatus(this)){
    		//TODO: Show connection prompt and ask to exit/retry
			Toast.makeText(getApplicationContext(), R.string.toast_connection_unavailable, 
                    Toast.LENGTH_LONG).show();
			return;
		}
		
		final AsyncHttpClient client = new AsyncHttpClient();
		
		PersistentCookieStore myCookieStore = new PersistentCookieStore(this);
		client.setCookieStore(myCookieStore);
		
		client.get("http://www.torchlight.in/mobile/logout", new AsyncHttpResponseHandler() {
		    
		    @Override
		    public void onFinish() {
		    	super.onFinish();
	    		onSignedout();
		    }
		});
	}

	private void onSignedIn(String cookie){
		mCookie = cookie;
		AppPreferences.set(this, AppPreferences.WEBCOOKIE, cookie);
		new WebViewTask().execute();
	}
	
	private void onSignedout(){
		AppPreferences.set(this, AppPreferences.WEBCOOKIE, "");
		Intent intent = new Intent(this, SigninActivity.class);
		startActivity(intent);
		finish();
	}
	
	private void onSigninFailed(){
		Toast.makeText(getApplicationContext(), 
                "Incorrect username and/or password", Toast.LENGTH_LONG).show();

		AppPreferences.set(this, AppPreferences.WEBCOOKIE, "");
		Intent intent = new Intent(this, SigninActivity.class);
		startActivity(intent);
		finish();
	}

	private void showSettingsActivity(){
		Intent intent = new Intent(this, SettingsActivity.class);
		startActivity(intent);
	}
	
	private void prepareMenu(){
    	/*try{
			InputStream is = getAssets().open("menu.json");
	    	int size = is.available();
	    	byte[] buffer = new byte[size];
	    	is.read(buffer);
	    	is.close();
	    	String json = new String(buffer, "UTF-8");
			
			makeMenu(json);
    	}catch(Exception ex){
			Log.w("Torchlight", "Failed to read menu from menu.json", ex);
		}*/
		
		final AsyncHttpClient client = new AsyncHttpClient();
		client.get("http://www.torchlight.in/mobile/sitemenu", new AsyncHttpResponseHandler() {
		    
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] response) {
				super.onSuccess(statusCode, headers, response);
		    	
				String s = new String(response);
		    	//Log.d("Torchlight", "response:" + s);
				//Log.d("Torchlight", "onSuccess: " + s);
		    	makeMenu(s);
			}
		});
		
	}
	
	private void makeMenu(String json){
		ArrayList<HashMap<String,String>> menuList = new ArrayList<HashMap<String,String>>();
		List<List<HashMap<String, String>>> childData = new ArrayList<List<HashMap<String, String>>>();
    	try{
    		JSONArray arr = new JSONArray(json);
    		int submenuIndex = mSubmenuData.size();
    		for (int i = 0; i < arr.length(); i++){
    			JSONObject o = arr.getJSONObject(i);
    			HashMap<String, String> hm = new HashMap<String,String>();
    			hm.put(LABEL, o.getString("title"));
    			String icon = o.getString("icon");
    			String[] tmp = icon.split("\\s+");
    			if (tmp[0].contentEquals("fa")){
    				hm.put(ICON, FontAwesome.getValue(this, tmp[1]));
    			}
    			hm.put(LINK, o.getString("link"));
    			menuList.add(hm);
    			
    			try{
    				JSONArray submenu = o.getJSONArray("submenu");
    				List<HashMap<String, String>> childList = new ArrayList<HashMap<String, String>>();
    				for (int j = 0; j < submenu.length(); j++){
    					JSONObject ob = submenu.getJSONObject(j);
    					HashMap<String, String> hm1 = new HashMap<String,String>();
    					hm1.put(LABEL, ob.getString("title"));
    					hm1.put(LINK, ob.getString("link"));
    					hm1.put(MenuAdapter.LAYOUT_KEY, Integer.toString(R.layout.menu_child_layout));
    					childList.add(hm1);
    				}
    				childData.add(childList);
    				hm.put(MenuAdapter.CHILD_INDEX_KEY, Integer.toString(submenuIndex));
    				submenuIndex++;
    			}catch(Exception ex){
    			}
    		}
    		
    		menuList.addAll(mMenuList);
    		childData.addAll(0, mSubmenuData);
    		mAdapter.updateData(menuList, childData);
    		
    	}catch(Exception ex){
    		Log.w("Torchlight", "Failed to parse menu from json", ex);
    	}
	}
	
	private class WebViewTask extends AsyncTask<Void, Void, Boolean> {
		String sessionCookie;
		CookieManager cookieManager;
 
	        @Override
		protected void onPreExecute() {
			CookieSyncManager.createInstance(MainActivity.this);
			cookieManager = CookieManager.getInstance();
			cookieManager.removeSessionCookie(); 
			super.onPreExecute();
		}
		protected Boolean doInBackground(Void... param) {
            /* this is very important - THIS IS THE HACK */
			SystemClock.sleep(1000);
			return false;
		}
		@Override
		protected void onPostExecute(Boolean result) {
			if (mCookie != null) {
				cookieManager.setCookie("www.torchlight.in", mCookie + ";domain=torchlight.in");
				CookieSyncManager.getInstance().sync();
			}
			WebSettings webSettings = mWebView.getSettings();
			webSettings.setJavaScriptEnabled(true);
			webSettings.setBuiltInZoomControls(true);
			/*mWebView.setWebViewClient(new WebViewClient() {
				@Override
				public boolean shouldOverrideUrlLoading(WebView view, String url) {
					return super.shouldOverrideUrlLoading(view, url);
				}
				
			});*/
			
			mWebView.loadUrl("http://www.torchlight.in/mobile/a/stores-manage");
		}
	}	
}
