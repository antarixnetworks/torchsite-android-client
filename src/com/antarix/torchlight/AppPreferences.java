package com.antarix.torchlight;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {

	private static final String FILENAME = "preferences";
	public static String USERNAME = "username";
	public static String PASSWORD = "password";
	public static String WEBCOOKIE = "webcookie";
	
	public static String get(Context context, String prop){
		SharedPreferences sharedPreferences = context.getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
		return sharedPreferences.getString(prop, "");
	}
	
	public static void set(Context context, String prop, String value){
		SharedPreferences sharedPreferences = context.getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(prop, value);
		editor.commit();
	}
}
