package com.antarix.torchlight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class MenuAdapter extends SimpleAdapter {

	private List<HashMap<String,String>> mList;
	private List<HashMap<String, String>> mRootList;
	private List<List<HashMap<String, String>>> mChildList;
	
	public static String CHILD_INDEX_KEY = "CHILDINDEX";
	public static String EXPANDED_KEY = "EXPANDED";
	public static String LAYOUT_KEY = "LAYOUT";
	private Context mContext;
	
	public MenuAdapter(Context context, List<HashMap<String, String>> rootData, 
			int layout, String[] from, int[] to, 
			List<List<HashMap<String, String>>> childData, int childLayout){
		super(context, rootData, layout, from, to);
		
		mContext = context;
		mList = rootData;
		mRootList = new ArrayList<HashMap<String,String>>();
		mRootList.addAll(rootData);
		mChildList = childData;
		
		initialize();
	}

	public void updateData(List<HashMap<String, String>> rootData, List<List<HashMap<String, String>>> childData){
		mRootList = new ArrayList<HashMap<String,String>>();
		mRootList.addAll(rootData);
		mChildList = childData;
		notifyDataSetChanged();
	}
	
	private void initialize(){
		mList.clear();
		for (int i = 0; i < mRootList.size(); i++){
			HashMap<String,String> item = (HashMap<String,String>)mRootList.get(i);
			mList.add(item);
			String childIndex = item.get(CHILD_INDEX_KEY);
			if (childIndex != null){
				if (item.containsKey(EXPANDED_KEY)){
					mList.addAll(mChildList.get(Integer.parseInt(childIndex)));
				}
			}
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View v = null;
		LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
		HashMap<String, String> item = (HashMap<String, String>)getItem(position);
		String layout = item.get(LAYOUT_KEY);  
		if (layout != null){
			convertView = layoutInflater.inflate(Integer.parseInt(layout), parent, false);
		}else{
			convertView = layoutInflater.inflate(R.layout.menu_item_layout, parent, false);
		}
		v = super.getView(position, convertView, parent);
		
		TextView cv = (TextView)v.findViewById(R.id.icon);
		Typeface tf = Typefaces.get(mContext, "fontawesome-webfont.ttf");
		cv.setTypeface(tf);
		
		return v;
	}
	
	@Override
	public void notifyDataSetChanged() {
		initialize();
		super.notifyDataSetChanged();
	}
}
